import Vue from 'vue'
import './plugins/bootstrap-vue'
import App from './App.vue'
import router from './router'
import store from './store/store'
import firebase from 'firebase'

Vue.config.productionTip = false

const firebaseConfig = {
    apiKey: "AIzaSyDHXcE-MDJP7sd-E0oYIiElMhOac8W0xAg",
    authDomain: "sportstore-81afc.firebaseapp.com",
    databaseURL: "https://sportstore-81afc.firebaseio.com",
    projectId: "sportstore-81afc",
    storageBucket: "sportstore-81afc.appspot.com",
    messagingSenderId: "221800257200",
    appId: "1:221800257200:web:19270c45892fd027"
};

// Initialize Firebase
firebase.initializeApp(firebaseConfig)

/* const database = firebase.database()

database.ref('items').on('value', snapshot => {
  console.log(snapshot.val())
}) */

new Vue({
    router,
    store,
    render: h => h(App)
}).$mount('#app')
