import Vue from 'vue'
import Vuex from 'vuex'
import {filterItems} from '../js/filterItems/filterItems'
import {LoadData} from '../data/LoadData'
import {LoadHorisontalMenuData} from '../data/LoadHorisontalMenuData'
import VuexPersist from 'vuex-persist'

Vue.use(Vuex)

const vuexLocalStorage = new VuexPersist({
    key: 'vuex',
    storage: window.localeStorage
})

export default new Vuex.Store({
    state: {
        articles: {},
        horisontalMenuData: {},
        filteredItems: {},
        shoppingCart: [],
        filters: {
            type: '',
            category: '',
            brand: [],
            size: ''
        }
    },

    mutations: {
        SET_STATE_CATEGORIES (state, horisontalMenuData) {
            state.horisontalMenuData = horisontalMenuData
        },

        SET_STATE_ARTICLES (state, articles) {
            state.articles = articles
        },

        SET_TO_SHOPPING_CART (state, order) {
            state.shoppingCart.push(order)
        },

        UPDATE_SHOPPING_CART (state, payload) {
            payload.articleInShoppingCart.quantity = Number(payload.articleInShoppingCart.quantity) + Number(payload.order.quantity)
        },

        BUY (state, orderMessage) {
            alert(orderMessage)
        },

        REMOVE_ARTICLE_SHOPPING_CART (state, articleToRemove) {
            // remove article with acording index from Shopping Cart
            state.shoppingCart.splice(state.shoppingCart.indexOf(articleToRemove), 1)
        },

        CLEAR_FILTERED_ITEMS (state) {
            state.filteredItems = {}
        },

        CLEAR_SHOPPING_CART (state) {
            state.shoppingCart = []
        },

        SET_FILTERS (state, filters) {
            Object.keys(filters).forEach(filter => {
                state.filters[filter] = filters[filter]
            })
        },

        SET_FILTERED_ITEMS (state, filteredItems) {
            state.filteredItems = filteredItems
        }
    },

    actions: {
        async loadHorisontalMenuData ({commit}, gender) {
            let categories = await LoadHorisontalMenuData(gender)

            commit('SET_STATE_CATEGORIES', categories)
        },

        async loadData ({commit}, gender) {
            let articles = await LoadData(gender)

            commit('SET_STATE_ARTICLES', articles)
        },

        addToShoppingCart ({commit}, order) {
            commit('SET_TO_SHOPPING_CART', order)
        },

        updateShoppingCart ({commit}, payload) {
            commit('UPDATE_SHOPPING_CART', payload)
        },

        buy ({commit, dispatch}, order) {
            let orderMessage = 'Request sent for '

            for (let i=0; i<order.length; i++) {
                orderMessage += order[i].name + ' size ' + order[i].size + ', '
            }
            orderMessage = orderMessage.substring(0, orderMessage.length -2)
            orderMessage += '.'

            commit('BUY', orderMessage)
            
            dispatch('clearShoppingCart')
        },

        filterItems ({state, commit, dispatch}, filters) {
            dispatch('setFilters', filters)
            dispatch('clearFilteredItems')

            const filteredItems = filterItems(state.articles, state.filters)

            commit('SET_FILTERED_ITEMS', filteredItems)
        },

        setFilters({commit}, filters) {
            commit('SET_FILTERS', filters)
        },

        removeArticle ({commit, getters}, article) {
            const articleToRemove = getters.articleInShoppingCart(article)

            commit('REMOVE_ARTICLE_SHOPPING_CART', articleToRemove)
        },

        clearFilteredItems ({commit}) {
            commit('CLEAR_FILTERED_ITEMS')
        },

        clearShoppingCart ({commit}) {
            commit('CLEAR_SHOPPING_CART')
        }
    },

  getters: {
    articleInShoppingCart: (state) => (order) => {
          return state.shoppingCart.find(article => {
              // looking for the article in shopping cart by checking its key(witch is id + size)
              return article.key === order.key
          })
      }
  },

  plugins: [vuexLocalStorage.plugin]
})
