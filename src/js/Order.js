class Order 
{
    constructor(article, size, quantity)
    {
        this.key = article.id + '#' + size,
        this.id = article.id,
        this.gender = article.gender,
        this.name = article.name,
        this.price = article.price,
        this.size = size,
        this.quantity = quantity,
        this.type = article.type,
        this.category = article.category,
        this.images = article.images
    }
}

export default Order
