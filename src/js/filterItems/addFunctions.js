export const addArticles = (items, itemsToFilter, type) => {
    
    Object.keys(itemsToFilter[type]).forEach(category => {
        Object.values(itemsToFilter[type][category]).forEach(article => {
            items.articles.push(article)
        })
    })
}

export const addCategory = (items) => {
    addElement(items.articles, items.categories, 'category')
}

export const addBrand = (items) => {
    addElement(items.articles, items.brand, 'brand')
}

export const addSize = (items) => {
    items.articles.forEach(article => {
        Object.keys(article.size).forEach(size => {
            if (!items.size.includes(size)) {
                items.size.push(size)
            }
        })
    })
}

export const addElement = (articles, items, element) => {
    articles.forEach(article => {
        if (!items.includes(article[element])) {
            items.push(article[element])
        }
    })
}
