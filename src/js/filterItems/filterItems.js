import * as filterFunc from './filterFunctions'
import * as addFunc from './addFunctions'

export const filterItems = (itemsToFilter, filters) => {
    let type, category, brand, size

    type = filters.type
    category = filters.category
    brand = filters.brand
    size = filters.size

    let items = {
        categories: [],
        articles: [],
        brand: [],
        size: []
    }

    filterActions(items, itemsToFilter, type, category, brand, size)

    Object.keys(items).forEach(item => {
        items[item].sort()
    })

    return items
}

const filterActions = (items, itemsToFilter, type, category, brand, size) => {
    addFunc.addArticles(items, itemsToFilter, type)

    filterFunc.filterByCategory(items, category)

    filterFunc.filterBySize(items, size)

    addFunc.addBrand(items)

    filterFunc.filterByBrand(items, brand)

    addFunc.addSize(items)

    addFunc.addCategory(items)    
}
