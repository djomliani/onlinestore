export const filterByCategory = (items, category) => {
    if (category) {
        items.articles = items.articles.filter(article => {
            return article.category === category
        })
    }
}

export const filterByBrand = (items, brand) => {
    if (brand.length !== 0) {
        items.articles = items.articles.filter(article => {
            return brand.includes(article.brand)
        })
    }
}

export const filterBySize = (items, size) => {
    if (size) {
        items.articles = items.articles.filter(article => {
             return Object.keys(article.size).includes(size)
        })
    }
}
