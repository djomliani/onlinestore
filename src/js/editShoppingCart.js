import store from '@/store/store'

export const editShoppingCart = (order) => {
    if (getGenderFromAvailableArticles(store.state.articles) !== order.gender) {
        alert('You have to choose ' + order.gender + ' articles again before changing quantity!')
        return
    }
    const articleInStorage = store.state.articles[order.type][order.category][order.id]
    
    const availableArticleQuantity = articleInStorage.size[order.size]

    const articleInShoppingCart = store.getters.articleInShoppingCart(order)

    if (!availableArticleQuantity) {
        alert('Ordered size or article you asked are no longer available! Please refresh the page!')
    } else if (articleInShoppingCart !== undefined) {
        
        // If sum of quantity of 'articleInShoppingCart' and quantity in 'order' is greater then 'availableArticleQuantity' alert the user
        if (Number(articleInShoppingCart.quantity) + Number(order.quantity) > availableArticleQuantity) {
            alert('There is only ' + availableArticleQuantity + ' left! You already reserved ' + articleInShoppingCart.quantity + ' in Shopping Cart.')
        } else {
            const payload = {articleInShoppingCart, order}
            store.dispatch('updateShoppingCart', payload)
        }
    } else {
        if (order.quantity > availableArticleQuantity) {
            alert('There is only ' + availableArticleQuantity + ' left!')
        } else {
            store.dispatch('addToShoppingCart', order)
        }
    }
}

const getGenderFromAvailableArticles = (articles) => {
    let type = Object.keys(articles)[0]
    let category = Object.keys(articles[type])[0]
    let article = Object.keys(articles[type][category])[0]
    let gender = articles[type][category][article].gender
    return gender
}
