export const sortingArray = (array, arrg) => {
    array.sort(article => {
        if (article.category === arrg) {
            return -1
        } else {
            return 1
        }
    })
}

export const objForFiltering = (type, category, brand, size) => {
    const filters =  {
        type: type,
        category: category,
        brand: brand,
        size: size
    }
    
    Object.keys(filters).forEach(
        filter => {
            
            if (typeof filters[filter] === 'undefined') {
                delete filters[filter]
            }
        }
    )

    return filters
}

export const getImgUrl = (article, img) => {
    let location = article.id

    let images = require.context('@/assets/', true)

    if (article.images) {
        return images('./' + location + '/' + img + ".jpg")
    } else {
        return images('./' + img + ".jpg")
    }
}
