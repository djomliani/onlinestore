import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import ArticlesList from './views/ArticlesListShow.vue'
import ArticleDetails from './views/ArticleDetailsShow.vue'
import store from './store/store'

Vue.use(Router)

export default new Router({
    routes: [
        {
            path: '/',
            name: 'home',
            component: Home
        },

        {
            path: '/about',
            name: 'about',
            // route level code-splitting
            // this generates a separate chunk (about.[hash].js) for this route
            // which is lazy-loaded when the route is visited.
            component: () => import(/* webpackChunkName: "about" */ './views/About.vue')
        },

        {
            path: '/shoppingCart',
            name: 'shoppingCart',
            // route level code-splitting
            // this generates a separate chunk (about.[hash].js) for this route
            // which is lazy-loaded when the route is visited.
            component: () => import(/* webpackChunkName: "about" */ './views/ShoppingCartShow.vue')
        },

        {
            path: '/:gender/:type/:category?',
            name: 'articlesList',
            component: ArticlesList,
            props: true,
            beforeEnter(to, from, next) {
                store.dispatch('loadData', to.params.gender)
                .then(() => next())
            }
        },

        {
            path: '/:gender/:type/:category/:id',
            name: 'articleDetails',
            component: ArticleDetails,
            props: true
        }      
    ],

    mode: 'history'
})
