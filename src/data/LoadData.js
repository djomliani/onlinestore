import firebase from 'firebase'

export const LoadData = async (gender = '') => {
    const snapshot = await firebase.database().ref('articals').child(gender).once('value');
    let articles = snapshot.val()
    Object.keys(articles).forEach(type => {
        Object.keys(articles[type]).forEach(category => {
            Object.keys(articles[type][category]).forEach(article => {
                articles[type][category][article] = {...articles[type][category][article], id: article, type: type, category: category}
            })
        })
    })
    return articles
}
