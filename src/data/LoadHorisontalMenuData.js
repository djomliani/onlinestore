import firebase from 'firebase'

export const LoadHorisontalMenuData = async (gender = '') => {
    const snapshot = await firebase.database().ref('articals').child(gender).once('value');
    let horisontalMenuData = {}
    let articles = snapshot.val()
    Object.keys(articles).forEach(type => {
        horisontalMenuData[type] = []
        Object.keys(articles[type]).forEach(category => {
            horisontalMenuData[type].push(category)
        })
    })
    return horisontalMenuData
}